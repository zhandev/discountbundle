<?php

namespace Cs\SomeProject\DiscountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cs\SomeProject\DiscountBundle\Entity\Abonent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SystemController extends Controller
{
    public function  getsystemprocentsAction()
    {
        $db = new Abonent();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $result = mssql_query('SELECT * FROM options');
        $row = mssql_fetch_object($result);
        $result = json_encode($row);

        return new Response($result);
    }

    public function indexAction()
    {
        return $this->render('DiscountBundle:System:index.html.twig');
    }

    public function updateprocentsAction(Request $request)
    {
        $db = new Abonent();
        $response = new Response();
        $request = Request::createFromGlobals();

        $pct_back_setting = $request->request->get('pct-back-setting');
        $pct_parent_firm_setting = $request->request->get('pct-parent-firm-setting');
        $pct_parent_person_setting = $request->request->get('pct-parent-person-setting');
        $pct_system_setting = $request->request->get('pct-system-setting');

        $query = 'UPDATE options SET [pct_back] = '.$pct_back_setting.', [pct_parent_firm] = '.$pct_parent_firm_setting.', [pct_parent_person] = '.$pct_parent_person_setting.', [pct_system] = '.$pct_system_setting .'   WHERE codeid = 1 ';

        if(mssql_query($query)) {
            $this->addFlash(
                'notice',
                'Системные проценты успешно обновлены!'
            );
        }else $result = 'bad';

        return $this->redirectToRoute('discount_system_settings');

    }
}