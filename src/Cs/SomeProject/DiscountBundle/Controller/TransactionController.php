<?php
/**
 * Created by PhpStorm.
 * User: zhanybek
 * Date: 10/13/15
 * Time: 10:28 AM
 */

namespace Cs\SomeProject\DiscountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cs\SomeProject\DiscountBundle\Entity\Transaction;

class TransactionController extends Controller
{
    public function indexAction()
    {
        $db = new Transaction();

        $sqlquery = mssql_query('SELECT * FROM [transaction] ORDER BY [codeid] DESC');

        $result = array();

        while ($row = mssql_fetch_object($sqlquery)){
            $result[]=$row;
        }
        return $this->render('DiscountBundle:Transaction:index.html.twig', array('transactions' => $result));
    }
}