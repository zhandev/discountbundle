<?php
/**
 * Created by PhpStorm.
 * User: zhanybek
 * Date: 10/13/15
 * Time: 10:17 AM
 */

namespace Cs\SomeProject\DiscountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Cs\SomeProject\DiscountBundle\Entity\Abonent;

class AbonentController extends Controller
{
    public function indexAction()
    {
        $db = new Abonent();

        $sqlquery = mssql_query('SELECT * FROM sp_user WHERE login > 99999  ORDER BY [codeid] DESC');

        $result = array();

        while ($row = mssql_fetch_array($sqlquery)){
            $result[]=$row;
        }

        return $this->render('DiscountBundle:Abonent:index.html.twig', array('users'=> $result));
    }

    public function createAction(Request $request)
    {
        $db = new Abonent();

        $request = Request::createFromGlobals();
        $codeid = $request->request->get('codeid');
        $user_parent = $request->request->get('parent');
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        $user_name = $request->request->get('user_name');

        if(mssql_query('INSERT INTO sp_user ([login], [password], [user_name], [type_user], [check_active_inn], [check_sms], [balans], [user_parent]) VALUES('.$login.', '.$password.',"'.$user_name.'", 1, 0, 0, 0,'.$user_parent.')')) {
            $this->addFlash(
                'notice',
                'Новый абонент успешно создан!'
            );
        }else {
            $this->addFlash(
                'notice',
                'Не удалось создать абонента!'
            );
        }

        return $this->redirectToRoute('discount_abonents');
    }

    public function deleteAction($id)
    {
        $db = new Abonent();

        if(mssql_query('DELETE FROM sp_user WHERE codeid = '.$id.' ')) {
            $this->addFlash(
                'notice',
                'Абонент удален!'
            );
        }else $result = 'bad';

        return $this->redirectToRoute('discount_abonents');
    }

    public function editAction(Request $request)
    {
        $request = Request::createFromGlobals();

        $codeid = (int)$request->request->get('codeid');
        $user_parent = (int)$request->request->get('parent');
        $login = addslashes($request->request->get('login'));
        $user_name =  $request->request->get('user_name');

        $query = 'UPDATE sp_user SET [login] = '.$login.', [user_name] = "'.addslashes($user_name).'", [user_parent] = '.$user_parent.'  WHERE codeid = '.$codeid.' ';
//        $query = iconv('utf-8', 'cp1251//IGNORE', $query);
//        print $query;exit();
        $db = new Abonent();
        if(mssql_query($query)) {
            $this->addFlash(
                'notice',
                'Данные абонента успешно изменены!'
            );
        }else $result = 'bad';

        return $this->redirectToRoute('discount_abonents');
    }



    public function getabonentAction(Request $request)
    {
        $request = Request::createFromGlobals();

        $codeid = $request->request->get('codeid');

        $db = new Abonent();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $result = mssql_query('SELECT * FROM sp_user WHERE codeid = '.$codeid.' ');
        $row = mssql_fetch_object($result);
        $result = json_encode($row);

        return new Response($result);
    }


    public function transactionAction( Request $request )
    {
        $request = Request::createFromGlobals();
        $login = $request->request->get('login');
        $login = $request->request->get('login');

        $response = new Response();

        $db = new Abonent();
        $sqlquery = mssql_query('SELECT * FROM [transaction] WHERE code_point = '.$login.' ORDER BY [codeid] DESC');


        $result = array();

        while ($row = mssql_fetch_array($sqlquery)){
            $result[]=$row;
        }

        $json = json_encode($result);
        $response->headers->set('Content-Type', 'application/json');
        return new Response($json);
    }

}