<?php
/**
 * Created by PhpStorm.
 * User: zhanybek
 * Date: 10/13/15
 * Time: 10:26 AM
 */

namespace Cs\SomeProject\DiscountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Cs\SomeProject\DiscountBundle\Entity\Abonent;

class PartnerController extends Controller
{
    public function indexAction()
    {
        $db = new Abonent();

        $sqlquery = mssql_query('SELECT * FROM sp_user WHERE login < 99999  ORDER BY [codeid] DESC');

        $result = array();

        while ($row = mssql_fetch_object($sqlquery)){
            $result[]=$row;
        }

        return $this->render('DiscountBundle:Partner:index.html.twig', array('users'=> $result));
    }

    public function createAction(Request $request)
    {
        $db = new Abonent();

        $request = Request::createFromGlobals();
        $codeid = $request->request->get('codeid');
        $user_parent = $request->request->get('parent');
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        $user_name = $request->request->get('user_name');

//        $main_parent = mssql_query('SELECT * FROM sp_user WHERE codeid = 5 ');
        $main_parent = mssql_query('SELECT * FROM sp_user WHERE login = 772333712 ');
        $main_parent = mssql_fetch_object($main_parent);

        if(mssql_query('INSERT INTO sp_user ([login], [password], [user_name], [type_user], [check_active_inn], [check_sms], [balans], [user_parent]) VALUES('.$login.', '.$password.',"'.$user_name.'", 2, 0, 0, 0, '.$main_parent->login.')')) {
            $this->addFlash(
                'notice',
                'Новый партнер успешно создан!'
            );
        }else $result = 'bad';

        return $this->redirectToRoute('discount_partners');
    }

    public function deleteAction($id)
    {
        $db = new Abonent();

        if(mssql_query('DELETE FROM sp_user WHERE codeid = '.$id.' ')) {
            $this->addFlash(
                'notice',
                'Партнер успешно удален!'
            );
        }else $result = 'bad';

        return $this->redirectToRoute('discount_partners');
    }

    public function editAction(Request $request)
    {
        $request = Request::createFromGlobals();

        $codeid = $request->request->get('codeid');
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        $user_name = $request->request->get('user_name');
        $procent_skidka = $request->request->get('procent_skidka');

        $db = new Abonent();

        if(mssql_query('UPDATE sp_user SET [login] = '.$login.', [password] = '.$password.', [user_name] = "'.$user_name.'", [procent_skidka] = '.$procent_skidka.' WHERE codeid = '.$codeid.' ')) {
            $this->addFlash(
                'notice',
                'Партнер успешно изменен!'
            );
        }else $result = 'bad';

        return $this->redirectToRoute('discount_partners');

    }

    public function getabonentAction(Request $request)
    {
        $request = Request::createFromGlobals();

        $codeid = $request->request->get('codeid');

        $db = new Abonent();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $result = mssql_query('SELECT * FROM sp_user WHERE codeid = '.$codeid.' ');
        $row = mssql_fetch_object($result);
        $result = json_encode($row);

        return new Response($result);
    }

    public function paidAction(Request $request)
    {
        $request = Request::createFromGlobals();

        $codeid = $request->request->get('codeid');
        $paid = $request->request->get('summa');

        $db = new Abonent();

        $sqlquery = mssql_query('SELECT balans, login FROM sp_user WHERE codeid = '.$codeid.' ');

        $row = mssql_fetch_row($sqlquery);

        $summa = $row['0'] - $paid;

        if(mssql_query('UPDATE sp_user SET [balans] = '.$summa.' WHERE codeid = '.$codeid.' ')) {
            $result = 'Ok!';
        }else $result = 'bad';

        $code_point = '772333712';
        $code_client = $row['1'];
        $amount = $paid;
        $status = 1;
        $confirm_code = 0;


        if(mssql_query('INSERT INTO [transaction] ([code_point], [code_client], [amount], [status], [confirm_code]) VALUES('.$code_point.', '.$code_client.','.$amount.', '.$status.', '.$confirm_code.' )')) {
            $this->addFlash(
                'notice',
                'Транзакция прошла успешно!'
            );
        }else {
            $this->addFlash(
                'notice',
                'Ошибка в создании транзакции!'
            );
        }


        $sqlquery1 = mssql_query('SELECT balans FROM sp_user WHERE login = 772333712 ');

        $row1 = mssql_fetch_row($sqlquery1);

        $summa1 = $row1['0'] + $paid;
        if(!mssql_query('UPDATE sp_user SET [balans] = '.$summa1.' WHERE login = 772333712 ')) {
            $this->addFlash(
                'notice',
                'Ошибка!'
            );
        }



        return $this->redirectToRoute('discount_partners');
    }
}