<?php
/**
 * Created by PhpStorm.
 * User: zhanybek
 * Date: 10/13/15
 * Time: 10:26 AM
 */

namespace Cs\SomeProject\DiscountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Cs\SomeProject\DiscountBundle\Entity\Abonent;

class PartnerController extends Controller
{
    public function indexAction()
    {
        $db = new Abonent();

        $sqlquery = mssql_query('SELECT * FROM sp_user WHERE login < 99999');

        $result = array();

        while ($row = mssql_fetch_object($sqlquery)){
            $result[]=$row;
        }

        return $this->render('DiscountBundle:Partner:index.html.twig', array('users'=> $result));
    }

    public function createAction(Request $request)
    {
        $db = new Abonent();

        $request = Request::createFromGlobals();
        $codeid = $request->request->get('codeid');
        $user_parent = $request->request->get('parent');
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        $user_name = $request->request->get('user_name');

        if(mssql_query('INSERT INTO sp_user ([login], [password], [user_name], [user_parent]) VALUES('.$login.', '.$password.',"'.$user_name.'", '.$user_parent.')')) {
            $result = 'Ok!';
        }else $result = 'bad';

        return $this->redirectToRoute('discount_partners');
    }

    public function deleteAction($id)
    {
        $db = new Abonent();

        if(mssql_query('DELETE FROM sp_user WHERE codeid = '.$id.' ')) {
            $result = 'Ok!';
        }else $result = 'bad';

        return $this->redirectToRoute('discount_partners');
    }

    public function editAction(Request $request)
    {
        $request = Request::createFromGlobals();

        $codeid = $request->request->get('codeid');
        $user_parent = $request->request->get('parent');
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        $user_name = $request->request->get('user_name');

        $db = new Abonent();

        if(mssql_query('UPDATE sp_user SET [login] = '.$login.', [password] = '.$password.', [user_name] = "'.$user_name.'", [user_parent] = '.$user_parent.'  WHERE codeid = '.$codeid.' ')) {
            $result = 'Ok!';
        }else $result = 'bad';

        return $this->redirectToRoute('discount_partners');

    }

    public function getabonentAction(Request $request)
    {
        $request = Request::createFromGlobals();

        $codeid = $request->request->get('codeid');

        $db = new Abonent();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $result = mssql_query('SELECT * FROM sp_user WHERE codeid = '.$codeid.' ');
        $row = mssql_fetch_object($result);
        $result = json_encode($row);

        return new Response($result);
    }
}