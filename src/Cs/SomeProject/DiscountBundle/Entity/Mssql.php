<?php
/**
 * Created by PhpStorm.
 * User: zhanybek
 * Date: 10/13/15
 * Time: 10:05 AM
 */

namespace Cs\SomeProject\DiscountBundle\Entity;

class Mssql
{
    public function __construct()
    {
        $db = mssql_connect('*******', 'sa', '*******');
        if ( !$db ) {
            throw new Exception('Ошибка подключения к ms sql.');
        }

        if ( !mssql_select_db('DISCOUNT_SYSTEM', $db) ) {
            throw new Exception('Ошибка подключения к базе данных');
        }
        return $db;
    }
}